describe('JIRA', function(){

  var jiraHostSpies = {};

  beforeAll(function(done){
    if (!window.AP) {
      AP = {
        '_hostModules': {},
        register: jasmine.createSpy('spy')
      };
    }

    AP._hostModules.jira = AP.jira = {
      openDatePicker: jasmine.createSpy('spy'),
      openCreateIssueDialog: jasmine.createSpy('spy'),
      _submitWorkflowConfigurationResponse: jasmine.createSpy('spy')
    };

    // preserve original method references for assertion purposes
    Object.keys(AP._hostModules.jira).forEach(function (key) {
      jiraHostSpies[key] = AP._hostModules.jira[key];
    });

    require(['jira'], function(jiraModule){
      done();
    });
  });

  beforeEach(function() {
    Object.keys(jiraHostSpies).forEach(function (key) {
      jiraHostSpies[key].calls.reset();
    });
  });


  it('extends the existing AP.jira api', function(){
    // check for overwriting our mocks.
    expect(jasmine.isSpy(AP.jira.openDatePicker)).toBe(false);
    expect(jasmine.isSpy(AP.jira.openCreateIssueDialog)).toBe(false);
  });

  describe('workflow', function(){
    it('extends the existing AP.jira with the workflow api', function(){
      expect(AP.jira.WorkflowConfiguration).toEqual(jasmine.any(Object));
      expect(AP.jira.WorkflowConfiguration.onSave).toEqual(jasmine.any(Function));
      expect(AP.jira.WorkflowConfiguration.onSaveValidation).toEqual(jasmine.any(Function));
      expect(AP.jira.WorkflowConfiguration.trigger).toEqual(jasmine.any(Function));
    });

    it('onSave callback is triggered when event is triggered', function(){
      var callback = jasmine.createSpy('spy');
      AP.jira.WorkflowConfiguration.onSaveValidation(jasmine.createSpy('spy').and.returnValue(true));
      AP.jira.WorkflowConfiguration.onSave(callback);
      AP.jira.WorkflowConfiguration.trigger();
      expect(callback.calls.count()).toEqual(1);
    });

    it('when triggered onSave value is returned as value', function(){
      var value = 'abc123';

      AP.jira.WorkflowConfiguration.onSaveValidation(jasmine.createSpy('spy').and.returnValue(true));
      AP.jira.WorkflowConfiguration.onSave(jasmine.createSpy('spy').and.returnValue(value));
      expect(AP.jira.WorkflowConfiguration.trigger().value).toEqual(value);
    });

    it('valid is true when workflow validation function returns true', function(){
      AP.jira.WorkflowConfiguration.onSaveValidation(jasmine.createSpy('spy').and.returnValue(true));
      expect(AP.jira.WorkflowConfiguration.trigger().valid).toEqual(true);
    });

    it('valid is false when workflow validation function returns false', function(){
      AP.jira.WorkflowConfiguration.onSaveValidation(jasmine.createSpy('spy').and.returnValue(false));

      expect(AP.jira.WorkflowConfiguration.trigger().valid).toEqual(false);
    });



});

describe('openDatePicker', function(){
  it('expects a position or element', function(){
    try {
      AP.jira.openDatePicker({
        onSelect: jasmine.createSpy('spy')
      });
    } catch (e){
      expect(/position/.test(e.message)).toBe(true);
      expect(/element/.test(e.message)).toBe(true);
    }
  });

    it('opens if only position is provided', function(){
      var onSelected = jasmine.createSpy('spy');
      expect(jiraHostSpies.openDatePicker.calls.count()).toEqual(0);
      AP.jira.openDatePicker({
        position: {
          top: 0,
          left: 0
        },
        date: "2011-12-13T15:20+01:00",
        onSelect: onSelected
      });
      expect(jiraHostSpies.openDatePicker.calls.count()).toEqual(1);
    });

    it('uses position of the element if both options.position and options.element are provided', function() {
      var onSelected = jasmine.createSpy('spy');
      var mockDiv = {
        nodeType: 1,
        getBoundingClientRect: function() {
          return {
            top: 900,
            left: 1000,
            height: 100
          };
        }
      };
      AP.jira.openDatePicker({
        element: mockDiv,
        position: {
          top: 123,
          left: 456
        },
        date: "2011-12-13T15:20+01:00",
        onSelect: onSelected
      });

      expect(jiraHostSpies.openDatePicker.calls.count()).toEqual(1);

      var firstCallOptions = jiraHostSpies.openDatePicker.calls.first().args[0];
      expect(firstCallOptions.position).toEqual({
        top: 1000,
        left: 1000
      });
    });

    it('opens if only element is provided', function() {
      var div = document.createElement("div");
      var onSelected = jasmine.createSpy('spy');

      AP.jira.openDatePicker({
        element: div,
        date: "2011-12-13T15:20+01:00",
        onSelect: onSelected
      });

      expect(jiraHostSpies.openDatePicker.calls.count()).toEqual(1);

    });

    it('throws exception onSelected callback is not provided', function() {
      var div = document.createElement("div");
      try {
        AP.jira.openDatePicker({
          element: div,
          date: "2011-12-13T15:20+01:00"
        });

      } catch(e){
        expect(e.message).toEqual('options.onSelect function is a required parameter.');
      }
      expect(jiraHostSpies.openDatePicker).not.toHaveBeenCalled();
    });
  });

});