describe('P2 compatibility shims', function () {
    beforeAll(function(done){
        if (!window.AP) {
            window.AP = {
                '_hostModules': {},
                register: jasmine.createSpy('spy')
            };
        }

        window.AP._hostModules._globals = {};

        window.AP._hostModules.user = {
            getUser: jasmine.createSpy('AP.user.getUser')
        };

        window.AP._hostModules.env = window.AP.env = {
            getLocation: jasmine.createSpy('AP._hostModules.env.getLocation'),
            sizeToParent: jasmine.createSpy('AP._hostModules.env.sizeToParent')
        };

        window.AP.history = {
            popState: jasmine.createSpy('AP._hostModules.history.popState')
        };

        const originalGetState = jasmine.createSpy('AP._hostModules.history.getState').and.callFake(function(cb) {
            cb('#someState');
        });
        const originalPushState = jasmine.createSpy('AP._hostModules.history.pushState');
        const originalReplaceState = jasmine.createSpy('AP._hostModules.history.replaceState');

        window.AP._hostModules.history = {
            getState: originalGetState,
            pushState: originalPushState,
            replaceState: originalReplaceState
        };

        window.AP._data = {};
        window.AP._data.options = {};

        window.AP._data.options.history = {
            state: '#someState'
        };

        window.AP._hostModules.dialog = {};
        window.AP._hostModules.Dialog = {};
        window.AP.dialog = {};
        window.AP.Dialog = {};
        require(['p2compat'], function(p2compat){
            done();
        });
    });


    describe('AP.env.getUser mapping to AP.user.getUser', function() {

        beforeEach(function() {
            AP._hostModules.user.getUser.calls.reset();
        });

        it('AP.getUser calls the correct method', function () {
            AP.getUser();
            expect(AP._hostModules.user.getUser).toHaveBeenCalled();
            expect(AP._hostModules.user.getUser.calls.count()).toEqual(1);
        });
        it('AP.env.getUser calls the correct method', function () {
            AP.env.getUser();
            expect(AP._hostModules.user.getUser).toHaveBeenCalled();
            expect(AP._hostModules.user.getUser.calls.count()).toEqual(1);
        });

        it('AP._hostModules.env.getUser calls the correct method', function () {
            AP._hostModules.env.getUser();
            expect(AP._hostModules.user.getUser).toHaveBeenCalled();
            expect(AP._hostModules.user.getUser.calls.count()).toEqual(1);
        });
    });

    describe('AP.env.getLocation mapping to AP.getLocation', function() {
        beforeEach(function() {
            AP._hostModules.env.getLocation.calls.reset();
        });

        it('AP.getLocation calls the correct method', function() {
            AP.getLocation();
            expect(AP._hostModules.env.getLocation).toHaveBeenCalled();
            expect(AP._hostModules.env.getLocation.calls.count()).toEqual(1);
        });

        it('AP._hostModules._globals.getLocation calls the correct method', function() {
            AP._hostModules._globals.getLocation();
            expect(AP._hostModules.env.getLocation).toHaveBeenCalled();
            expect(AP._hostModules.env.getLocation.calls.count()).toEqual(1);
        });
    });

    describe('AP.env.sizeToParent mapping to AP.sizeToParent', function() {
        beforeEach(function() {
            AP._hostModules.env.sizeToParent.calls.reset();
        });

        it('AP.sizeToParent calls the correct method', function() {
            AP.sizeToParent();
            expect(AP._hostModules.env.sizeToParent).toHaveBeenCalled();
            expect(AP._hostModules.env.sizeToParent.calls.count()).toEqual(1);
        });

        it('AP._hostModules._globals.sizeToParent calls the correct method', function() {
            AP._hostModules._globals.sizeToParent();
            expect(AP._hostModules.env.sizeToParent).toHaveBeenCalled();
            expect(AP._hostModules.env.sizeToParent.calls.count()).toEqual(1);
        });
    });

    describe('AP.dialog shims', function() {
        describe('isCloseOnEscape', function() {
            it('is defined', function() {
                expect(typeof AP._hostModules.dialog.isCloseOnEscape).toEqual('function');
                expect(typeof AP._hostModules.Dialog.isCloseOnEscape).toEqual('function');
                expect(typeof AP.dialog.isCloseOnEscape).toEqual('function');
                expect(typeof AP.Dialog.isCloseOnEscape).toEqual('function');
            });

            it('accepts and calls a callback method', function() {
                AP._data.options.preventDialogCloseOnEscape = true;
                const callback = jasmine.createSpy('callback');
                AP._hostModules.dialog.isCloseOnEscape(callback);
                expect(callback).toHaveBeenCalled();
                expect(callback).toHaveBeenCalledWith(false);
            })
        });
    });
});