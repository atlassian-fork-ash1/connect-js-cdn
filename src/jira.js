/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 *                                                                                             *
 *   (                                                                                         *
 *   )\ )                                       )       (                                      *
 *  (()/(     (         (      (          )  ( /(   (   )\ )                                   *
 *   /(_))   ))\ `  )   )(    ))\  (   ( /(  )\()) ))\ (()/(                                   *
 *  (_))_   /((_)/(/(  (()\  /((_) )\  )(_))(_))/ /((_) ((_))                                  *
 *   |   \ (_)) ((_)_\  ((_)(_))  ((_)((_)_ | |_ (_))   _| |                                   *
 *   | |) |/ -_)| '_ \)| '_|/ -_)/ _| / _` ||  _|/ -_)/ _` |                                   *
 *   |___/ \___|| .__/ |_|  \___|\__| \__,_| \__|\___|\__,_|                                   *
 *              |_|                                                                            *
 *                                                                                             *
 *  The code added to iframe.js coming from simple-xdm is for backwards compatibility only     *
 *  No new code may be added to plugin/* All modules must be defined in the host product       *
 *  For examples on how to extend ACJS please see https://bitbucket.org/atlassian/simple-xdm   *
 *                                                                                             *
 *        " No changes necessary in the iframe library when the host API changes "             *
 *                                                                                             *
 *  also see https://bitbucket.org/atlassian/atlassian-connect-js and host/*                   *
 *                                                                                             *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

(function(){
  "use strict";
  if(!AP.jira){
    return; // we're not in jira.
  }

  var workflowListener,
      validationListener,
      dashboardItemEditListener = function(){},
      dateSelectedListener;

  function isFunction(arg){
    return (typeof arg === "function");
  }

  function isDomElement(el) {
    return el && el.nodeType && el.nodeType == 1;
  }

  /**
   * @class WorkflowConfiguration
   * @product Jira
   */
  var WorkflowConfiguration = {
    /**
    * Retrieves a workflow configuration object.
    *
    * @param {WorkflowConfiguration} callback - the callback that handles the response.
    */

    /**
     * Validate a workflow configuration before saving
     * @noDemo
     * @memberOf WorkflowConfiguration
     * @param {Function} listener called on validation. Return false to indicate that validation has not passed and the workflow cannot be saved.
     */
    onSaveValidation: function (listener) {
      validationListener = listener;
    },
    /**
     * Attach a callback function to run when a workflow is saved
     * @noDemo
     * @memberOf WorkflowConfiguration
     * @param {Function} listener called on save.
     */
    onSave: function (listener) {
        workflowListener = listener;
    },
    /**
     * Save a workflow configuration if valid.
     * @noDemo
     * @memberOf WorkflowConfiguration
     * @returns {WorkflowConfigurationTriggerResponse} An object Containing `{valid, value}` properties.valid (the result of the validation listener) and value (result of onSave listener) properties.
     */
    trigger: function () {
      var valid = true;
      if (isFunction(validationListener)) {
        valid = validationListener.call();
      }
      /**
       * An object returned when the <a href="../workflowconfiguration/">WorkflowConfiguration</a> trigger method is invoked.
       * @name WorkflowConfigurationTriggerResponse
       * @class
       * @property {Boolean} valid The result of the validation listener <a href="../workflowconfiguration/#onSaveValidation">WorkflowConfiguration.onSaveValidation</a>.
       * @property {*} value The result of the <a href="../workflowconfiguration/#onSave">WorkflowConfiguration.onSave</a>.
       */
      var workflowListenerValue;
      if(workflowListener){
        workflowListenerValue = workflowListener.call();
      }
      var response = {
        valid: valid,
        value: valid ? "" + workflowListenerValue : undefined
      };
      AP.jira._submitWorkflowConfigurationResponse(response);
      return response;
    }
  };

  AP.register({
    jira_workflow_post_function_submit: function() {
      WorkflowConfiguration.trigger();
    }
  });

  AP.jira.WorkflowConfiguration = AP._hostModules.jira.WorkflowConfiguration = WorkflowConfiguration;

  /**
   * @class DashboardItem
   */
  AP.register({
    jira_dashboard_item_edit: function() {
      dashboardItemEditListener.call();
    }
  });

  /**
   * Attach a callback function to run when user clicks 'edit' in the dashboard item's menu
   * @noDemo
   * @memberOf DashboardItem
   * @method onDashboardItemEdit
   * @param {Function} listener called on dashboard item edit.
   */
  function onDashboardItemEdit(listener){
    dashboardItemEditListener = listener;
  }

 AP.jira.DashboardItem = AP._hostModules.jira.DashboardItem = {onDashboardItemEdit: onDashboardItemEdit};

  /**
   * @class DatePicker~position
   * @property {number} top - Distance in pixels from the top edge of the iframe date picker should be shown at.
   * @property {number} left - Distance in pixels from the left edge of the iframe date picker should be shown at.
   */

  /**
   * @class DatePicker~options
   * @property {HTMLElement} element - HTML element below which date picker will be positioned. If provided, it takes precedence over `options.position`.
   * @property {DatePicker~position} position - Position of the element relative to the iframe. options.element takes precedence over it when provided.
   * @property {Boolean} showTime - Flag determining whether the component should also have a time picker. Defaults to `false`.
   * @property {String} date - <p>Date (and time) that should be pre-selected when displaying the picker in the format understandable by Date.parse method in JavaScript.</p>
   * <p>ISO 8601 is preferred. Timezone should be set to Z for UTC time or in the format of +/-hh:mm. Not setting it will cause JavaScript to use local timezone set in the browser. Defaults to current date/time.</p>
   * @property {Function} onSelect - Callback that will be invoked when the date (and time) is selected by the user.
   */

  /**
   * Shows a date picker component. A callback will be invoked when the date (and time) is selected by the user.
   *
   * @param {DatePicker~options} options - Configuration of the date picker.
   *
   * @noDemo
   * @example
   * var dateField = document.querySelector("#date-field");
   * var dateTrigger = document.querySelector("#date-trigger");
   *
   * dateTrigger.addEventListener("click", function(e) {
   *   e.preventDefault();
   *   AP.jira.openDatePicker({
   *     element: dateTrigger,
   *     date: "2011-12-13T15:20+01:00",
   *     showTime: true,
   *     onSelect: function (isoDate, date) {
   *       dateField.value = date;
   *       dateField.setAttribute("data-iso", isoDate);
   *       dateField.focus();
   *     }
   *   });
   * });
   */
  var original_openDatePicker = AP._hostModules.jira.openDatePicker.bind({});
  AP.jira.openDatePicker = AP._hostModules.jira.openDatePicker = function (options) {
    // options = options || {};
    // can this if be moved?
    if (!options.position || typeof options.position !== "object") {
      if (!isDomElement(options.element)) {
          throw new Error("Providing either options.position or options.element is required.");
      }
      options.position = {};
    }

    if (!options.onSelect || !isFunction(options.onSelect)) {
      throw new Error("options.onSelect function is a required parameter.");
    }

    var sanitisedOptions = {
      element: options.element,
      position: {
        top: options.position.top || 0,
        left: options.position.left || 0
      },
      date: options.date,
      showTime: !!options.showTime,
      onSelect: options.onSelect
    };

    var elBoundingBox;
    dateSelectedListener = sanitisedOptions.onSelect;
    delete sanitisedOptions.onSelect;

    if (sanitisedOptions.element) {
      elBoundingBox = sanitisedOptions.element.getBoundingClientRect();
      sanitisedOptions.position = {
        left: elBoundingBox.left,
        top: elBoundingBox.top + elBoundingBox.height
      };
      delete sanitisedOptions.element;
    }

    original_openDatePicker(sanitisedOptions);
  };

  AP.register({
    jira_date_selected: function(data) {
      if (isFunction(dateSelectedListener)) {
        dateSelectedListener.call({}, data.isoDate, data.date);
      }
    }
  });

  var original_openCreateIssueDialog = AP._hostModules.jira.openCreateIssueDialog.bind({});
  AP.jira.openCreateIssueDialog = AP._hostModules.jira.openCreateIssueDialog = function (callback, fields) {
    original_openCreateIssueDialog(fields, callback);
  };
}());