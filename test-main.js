var allTestFiles = []
var TEST_REGEXP = /(spec|test)\.js$/i
var SOURCE_REGEXP = /base\/src\/([a-z0-9]*)/
var allSourcePaths = {};
// Get a list of all the test files to include
Object.keys(window.__karma__.files).forEach(function (file) {
  if (TEST_REGEXP.test(file)) {
    // Normalize paths to RequireJS module names.
    // If you require sub-dependencies of test files to be loaded as-is (requiring file extension)
    // then do not normalize the paths
    var normalizedTestModule = file.replace(/^\/base\/|\.js$/g, '')
    allTestFiles.push(normalizedTestModule)
  }
  if(SOURCE_REGEXP.test(file)) {
    var aMatch = file.match(SOURCE_REGEXP);
    allSourcePaths[aMatch[1]] = file.replace('.js', '');
  }
})

require.config({
  // Karma serves files under /base, which is the basePath from your config file
  baseUrl: '/base',

  // dynamically load all test files
  deps: allTestFiles,

  // we have to kickoff jasmine, as it is asynchronous
  callback: window.__karma__.start,
  paths: allSourcePaths
})
